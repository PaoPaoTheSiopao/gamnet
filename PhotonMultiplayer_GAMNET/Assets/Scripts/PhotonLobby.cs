﻿using Photon.Realtime;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public static PhotonLobby lobby;

    public GameObject battleButton;
    public GameObject cancelButton;

    private void Awake()
    {
        lobby = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to the Photon master server");
        PhotonNetwork.AutomaticallySyncScene = true;
        battleButton.SetActive(true);
    }

    public void OnBattleButtonClick()
    {
        Debug.Log("BattleButtonClick");
        battleButton.SetActive(false);
        cancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join a Random Room But Failed");
        CreateRoom();
    }

    void CreateRoom()
    {
        Debug.Log("CreatingRoom");
        int randomRoomName = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 10 };
        PhotonNetwork.CreateRoom("Room " + randomRoomName, roomOps);
    }

    
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to createRoom, Trying to create again");
        CreateRoom();
    }
    
    public void OnCancelButtonClick()
    {
        Debug.Log("CancelButtonClick");
        cancelButton.SetActive(false);
        battleButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
