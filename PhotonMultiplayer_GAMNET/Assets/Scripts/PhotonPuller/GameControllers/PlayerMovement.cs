﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private PhotonView PV;
    private CharacterController myCC;
    private Rigidbody rb;

    private bool isGrounded;

    public float movementSpeed;
    public float rotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        myCC = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(PV.IsMine)
        {
            BasicMovement();
            if(Input.GetKeyDown(KeyCode.Space)&&isGrounded)
            {
                Jump();
            }
            //BasicRotation();
        }
    }

    void Jump()
    {
        rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        isGrounded = false;
    }

    void BasicMovement()
    {
        if(Input.GetKey(KeyCode.W))
        {
            //myCC.Move(transform.forward * Time.deltaTime * movementSpeed);
            transform.Translate(0, 0, movementSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            //myCC.Move(transform.right * Time.deltaTime * movementSpeed);
            transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            //myCC.Move(-transform.forward * Time.deltaTime * movementSpeed);
            transform.Translate(0, 0, -movementSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            //myCC.Move(-transform.right * Time.deltaTime * movementSpeed);
            transform.Rotate(0, -rotationSpeed * Time.deltaTime, 0);
        }
    }

    void BasicRotation()
    {
        float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed;
        transform.Rotate(new Vector3(0, mouseX, 0));
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Floor")
        {
            isGrounded = true;
        }
    }
}
