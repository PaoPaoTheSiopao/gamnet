﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharHealth : MonoBehaviour
{

    public float defHealth;
    [SerializeField]private float mainHealth;

    public Image healthBar;

    // Start is called before the first frame update
    void Start()
    {
        mainHealth = defHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damage)
    {
        mainHealth -= damage;
        healthBar.fillAmount = mainHealth / defHealth;
        if(mainHealth<=0)
        {
            GameObject gmsp = GameObject.FindGameObjectWithTag("GS");
            gmsp.GetComponent<GameSetup>().DisconnectPlayer();
            Destroy(this.gameObject);
        }
    }
}
