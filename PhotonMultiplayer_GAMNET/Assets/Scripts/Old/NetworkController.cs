﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkController : MonoBehaviourPunCallbacks
{

    //Documentation : https://doc.photonengine.com/en-us/pun/current/getting-started/pun-intro
    //API Scripting : https://doc-api.photonengine.com/en/pun/v2/index.html

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to region : " + PhotonNetwork.CloudRegion + " server");
        //base.OnConnectedToMaster();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
